import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class homework {
	private static PrintWriter out;
	private static File createFile;
	
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		// TODO Auto-generated method stub
		String fileName = "";
		try {
				readInputFile("input.txt");
		  	}
		  	catch (FileNotFoundException exc) {
		  		System.out.println("File not found: " + fileName);
		  	}
		  	catch (IOException exc) {
		  		exc.printStackTrace();
		  	}
		long endTime = System.currentTimeMillis();
		System.out.println("Took "+(endTime - startTime) + " ms"); 
	}
	private static void readInputFile(String fileName) throws IOException {
		
		String Mode = "";
		int boardSize = 0;
		String role = "";
		String opponent = "";
		int depth = 0;
		Node[][] board = null;
		Player ours = null;
		Player theirs = null;
		FileReader file = new FileReader(fileName);
		BufferedReader br = new BufferedReader(file);
		String line = null;
		// read first line 
		if ((line = br.readLine()) !=null){
		    String [] sizeInput = line.split("\\s+");
		    boardSize = Integer.valueOf(sizeInput[0]);      //read size of board
		    board = new Node[boardSize][boardSize];
		    //System.out.println(boardSize);
		}
		// read Mode
		if ((line = br.readLine()) !=null){
			String [] modeInput = line.split("\\s+");
			Mode = modeInput[0];
		}
		// read turn
		if ((line = br.readLine()) !=null){
			//line = br.readLine();
			String [] turnInput = line.split("\\s+");
			role = turnInput[0];
			if (role.equals("X"))
				opponent = "O";
			else 
				opponent = "X";
			ours = new Player(role);
			theirs = new Player(opponent);
		}
		// read depth
		if ((line = br.readLine()) !=null){
			String [] depthInput = line.split("\\s+");
			depth = Integer.valueOf(depthInput[0]);
		}
		// read all board value then initialize its value
		for (int i = 0; i<boardSize;i++){
			if ((line = br.readLine()) !=null){
				String [] colValueInput = line.split("\\s+");
				initializeBoardValue (i, colValueInput, board, boardSize);
			}
		}
		for (int i = 0; i<boardSize;i++){
			if ((line = br.readLine()) !=null){
				String [] colPlayer = new String[boardSize];
				for (int j = 0; j <boardSize;j++){
					colPlayer[j] = String.valueOf(line.charAt(j));
				}
				initializeBoardPlayer (i, colPlayer, board, boardSize, ours, theirs);
			}
		}
		br.close();
		try {
			createFile = new File ("output.txt");
			out = new PrintWriter(createFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switch (Mode){
			case "MINIMAX":
				int[] result_minimax = minimax(board, depth, true, ours, theirs);
				out.println(getCharForNumber(result_minimax[2]) + (result_minimax[1]+1) + " " + getAction(result_minimax[3]));
				//System.out.println(getCharForNumber(result_minimax[2]) + (result_minimax[1]+1) + " " + getAction(result_minimax[3]));
				if (result_minimax[3]==2)
					board[result_minimax[1]][result_minimax[2]].setPlayer(ours.name);
				else 
					RaidOperation(board, new int[]{result_minimax[1], result_minimax[2]}, ours);
				for (int i = 0; i<board.length-1;i++){
					for (int j = 0; j<board[0].length;j++){
						if (board[i][j].isOccupied()){
							out.print(board[i][j].player);
						}
						else {
							out.print(".");
						}
					}
					out.println();
				}
				for (int i = 0; i<board[0].length;i++){
					if (board[board.length-1][i].isOccupied()){
						out.print(board[board.length-1][i].player);
					}
					else out.print(".");
				}
				out.close();
			break;
			case "ALPHABETA":
				int[] result_alphabeta = alphabeta(board, depth, true, ours, theirs, Integer.MIN_VALUE, Integer.MAX_VALUE);
				out.println(getCharForNumber(result_alphabeta[2]) + (result_alphabeta[1]+1) + " " + getAction(result_alphabeta[3]));
				//System.out.println(getCharForNumber(result_alphabeta[2]) + (result_alphabeta[1]+1) + " " + getAction(result_alphabeta[3]));
				if (result_alphabeta[3]==2)
					board[result_alphabeta[1]][result_alphabeta[2]].setPlayer(ours.name);
				else 
					RaidOperation(board, new int[]{result_alphabeta[1], result_alphabeta[2]}, ours);
				for (int i = 0; i<board.length-1;i++){
					for (int j = 0; j<board[0].length;j++){
						if (board[i][j].isOccupied()){
							out.print(board[i][j].player);
						}
						else {
							out.print(".");
						}
					}
					out.println();
				}
				for (int i = 0; i<board[0].length;i++){
					if (board[board.length-1][i].isOccupied()){
						out.print(board[board.length-1][i].player);
					}
					else out.print(".");
				}
				out.close();
				break;
		}
	}
	private static String getAction(int i ){
		return (i>1)? "Stake":"Raid";
	}
	private static String getCharForNumber(int i) {
	    return i >= 0 && i < 27 ? String.valueOf((char)(i + 65)) : null;
	}
	// minimax implementation
	private static int[] minimax(Node[][] board, int depth, boolean turn, Player computer, Player opponent) {
		
	      List<int[]> nextMoves = generateMoves(board);
	      int bestScore = (turn) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
	      int currentScore;
	      int bestRow = -1;
	      int bestCol = -1;
	      int action = 0;
	      int tempAction = 0; // 2 = stake, 1 = raid
	      
	      if (nextMoves.isEmpty() || depth == 0) {
	         bestScore = evaluate(board, computer);
	         //return new int[] {bestScore, bestRow, bestCol, action};
	         //System.out.println("best score " + bestScore);
	      } else {
	    	  //do stake
	    	  for (int[] move : nextMoves) {
	              Node[][] copy = new Node[board.length][board.length];
	    		  for (int i = 0; i<board.length;i++){
	    			  for (int j = 0;j<board.length;j++){
	    				  copy[i][j] = new Node();
	    				  copy[i][j].player = board[i][j].player;
	    				  copy[i][j].occupied = board[i][j].occupied;
	    				  copy[i][j].value = board[i][j].value;
	    			  }
	    		  }
	          	  if (!copy[move[0]][move[1]].isOccupied()){
	          		  if (turn) { 
	        		  	  //do stake operation
	        			  copy[move[0]][move[1]].setPlayer(computer.name);
	        			  tempAction = 2;
	        			  currentScore = minimax(copy, depth - 1, false, computer, opponent)[0];
	        			  if (currentScore > bestScore) {
	       	                  bestScore = currentScore;
	       	                  bestRow = move[0];
	       	                  bestCol = move[1];
	       	                  action = tempAction;
	       	                  //.out.println(bestScore + " computer turn");
	       	              }
	        		  } 
	          		  else {   //do stake operation
	      				  copy[move[0]][move[1]].setPlayer(opponent.name);
	      				  tempAction = 1;
	      				  currentScore = minimax(copy, depth - 1, true, computer, opponent)[0];
	          			  if (currentScore == bestScore && tempAction>action) {  // handle with tie-breaking 
	       	                  bestScore = currentScore;
	       	                  bestRow = move[0];
	       	                  bestCol = move[1];
	       	                  action = tempAction;
	       	                  //.out.println(bestScore + " computer turn");
	          			  }
	          			  if (currentScore < bestScore) {
	       	                 bestScore = currentScore;
	       	                 bestRow = move[0];
	       	                 bestCol = move[1];
	       	                 action = tempAction;
	       	                 //System.out.println(bestScore + " opponent turn");
	       	              }
	            		   
	 	              }
	          	  }
	    	  	}
	    	  	// do raid operation
		    	  for (int[] move : nextMoves) {
		              Node[][] copy = new Node[board.length][board.length];
		    		  for (int i = 0; i<board.length;i++){
		    			  for (int j = 0;j<board.length;j++){
		    				  copy[i][j] = new Node();
		    				  copy[i][j].player = board[i][j].player;
		    				  copy[i][j].occupied = board[i][j].occupied;
		    				  copy[i][j].value = board[i][j].value;
		    			  }
		    		  }
	          	  if (!copy[move[0]][move[1]].isOccupied()){
	          		  if (turn) {  
	      			  // check stake and raid operation
	            		  if (checkRaid(copy, move, computer)){
	            			  //do raid operation
	            			  //System.out.println("do raid");
	            			  RaidOperation(copy, move, computer);
	            			  tempAction = 1;
	            			  currentScore = minimax(copy, depth - 1, false, computer, opponent)[0];
	            			 
		       	              if (currentScore > bestScore) {
		       	                  bestScore = currentScore;
		       	                  bestRow = move[0];
		       	                  bestCol = move[1];
		       	                  action = tempAction;
		       	                  //.out.println(bestScore + " computer turn");
		       	              }
	            		  }
		            		  
	     	          } 
	          		  else {  
	          			  if (checkRaid(copy, move, opponent)){
	            			  //do raid operation
	            			  RaidOperation(copy, move, opponent);
	            			  tempAction = 2;
	            			  //board[move[0]][move[1]].setPlayer(opponent.name);
	            			  currentScore = minimax(copy, depth - 1, true, computer, opponent)[0];
	            			  if (currentScore < bestScore) {
		       	                 bestScore = currentScore;
		       	                 bestRow = move[0];
		       	                 bestCol = move[1];
		       	                 action = tempAction;
		       	                 //System.out.println(bestScore + " opponent turn");
		       	              }
	            		  }
	     	          }
	          	  }
	          }
	      }
	      return new int[] {bestScore, bestRow, bestCol, action};
	}
	private static int[] alphabeta(Node[][] board, int depth, boolean turn, Player computer, Player opponent, int alpha, int beta) {
		  List<int[]> nextMoves = generateMoves(board);
	      int currentScore;
	      int bestRow = -1;
	      int bestCol = -1;
	      int tempAction = 0; 
	      int action = 0;
	      if (nextMoves.isEmpty() || depth == 0) {
	          currentScore = evaluate(board, computer);  
	    	  return new int[] {currentScore,  bestRow, bestCol, action};
	      } else {
	    	  // do stake first
	    	  for (int[] move : nextMoves) {
	              Node[][] copy = new Node[board.length][board.length];
	    		  for (int i = 0; i<board.length;i++){
	    			  for (int j = 0;j<board.length;j++){
	    				  copy[i][j] = new Node();
	    				  copy[i][j].player = board[i][j].player;
	    				  copy[i][j].occupied = board[i][j].occupied;
	    				  copy[i][j].value = board[i][j].value;
	    			  }
	    		  }
	          	  if (!copy[move[0]][move[1]].isOccupied()){
	          		  if (turn) {  
	      			    //do stake operation
	        			  copy[move[0]][move[1]].setPlayer(computer.name);
	        			  tempAction = 2;
	        			  currentScore = alphabeta(copy, depth - 1, false, computer, opponent, alpha, beta)[0];
	        			  if (currentScore > alpha) {
	       	                  alpha = currentScore;
	       	                  bestRow = move[0];
	       	                  bestCol = move[1];
	       	                  action = tempAction;
	       	                  //.out.println(bestScore + " computer turn");
	       	              } 
	 	              } 
	          		  else {  
	          			  //do stake operation
	      				  copy[move[0]][move[1]].setPlayer(opponent.name);
	      				  tempAction = 2;
	      				  currentScore = alphabeta(copy, depth - 1, true, computer, opponent, alpha, beta)[0];
	          			  if (currentScore < beta) {
	       	                 beta = currentScore;
	       	                 bestRow = move[0];
	       	                 bestCol = move[1];
	       	                 action = tempAction;
	       	                 //System.out.println(bestScore + " opponent turn");
	       	              }
	 	              }
	          		  // cut-off function
	          		  if (alpha >= beta) break;
	          	  }  
	    	  } 	
	    	  // do raid operation
	    	  for (int[] move : nextMoves) {
	              Node[][] copy = new Node[board.length][board.length];
	    		  for (int i = 0; i<board.length;i++){
	    			  for (int j = 0;j<board.length;j++){
	    				  copy[i][j] = new Node();
	    				  copy[i][j].player = board[i][j].player;
	    				  copy[i][j].occupied = board[i][j].occupied;
	    				  copy[i][j].value = board[i][j].value;
	    			  }
	    		  }
          	  if (!copy[move[0]][move[1]].isOccupied()){
          		  if (turn) {  
          			  // check stake and raid operation
	            		  if (checkRaid(copy, move, computer)){
	            			  //do raid operation
	            			  //System.out.println("do raid");
	            			  RaidOperation(copy, move, computer);
	            			  tempAction = 1;
	            			  currentScore = alphabeta(copy, depth - 1, false, computer, opponent, alpha, beta)[0];
	            			  if (currentScore > alpha) {
		       	                  alpha = currentScore;
		       	                  bestRow = move[0];
		       	                  bestCol = move[1];
		       	                  action = tempAction;
		       	                  //.out.println(bestScore + " computer turn");
		       	              }
	            		  }
	            		  
     	              } 
          		  else { 
          			  if (checkRaid(copy, move, opponent)){
            			  //do raid operation
            			  RaidOperation(copy, move, opponent);
            			  tempAction = 1;
            			  //board[move[0]][move[1]].setPlayer(opponent.name);
            			  currentScore = alphabeta(copy, depth - 1, true, computer, opponent, alpha, beta)[0];
            			  if (currentScore < beta) {
	       	                 beta = currentScore;
	       	                 bestRow = move[0];
	       	                 bestCol = move[1];
	       	                 action = tempAction;
	       	                 //System.out.println(bestScore + " opponent turn");
	       	              }
            		  }
      			  
 	              }
          		  // cut-off function
          		  if (alpha >= beta) break;
          	  }
	    	  } 	
	      }
	      return new int[] {(turn)? alpha:beta,  bestRow, bestCol, action};
	}
	private static void RaidOperation(Node[][] board, int[] move, Player player){
		board[move[0]][move[1]].setPlayer(player.name);
		if (move[0]-1>=0){   // check left
			if (board[move[0]-1][move[1]].isOccupied()){
				board[move[0]-1][move[1]].setPlayer(player.name);
			}
		}
		if (move[0]+1<=board.length-1){  // check right
			if (board[move[0]+1][move[1]].isOccupied()){
				board[move[0]+1][move[1]].setPlayer(player.name);
			}
		}
		if (move[1]-1>=0){  // check top
			if (board[move[0]][move[1]-1].isOccupied()){
				board[move[0]][move[1]-1].setPlayer(player.name);
			}
		}
		if (move[1]+1<=board.length-1){  // check bottom
			if (board[move[0]][move[1]+1].isOccupied()){
				board[move[0]][move[1]+1].setPlayer(player.name);
			}
		}
	}
	/*private static boolean checkStake(Node[][] board, int[] move, Player player){
		if (move[0]-1>=0){   // check left
			if (board[move[0]-1][move[1]].isOccupied()&&board[move[0]-1][move[1]].player.equals(player.name)){
				// should check adjacent 
				if (checkRaidHelper(board, new int[] {move[0], move[1]},  player)){
					return false;
				}
			}
		}
		if (move[0]+1<=board.length-1){  // check right
			if (board[move[0]+1][move[1]].isOccupied()&&board[move[0]+1][move[1]].player.equals(player.name)){
				if (checkRaidHelper(board, new int[] {move[0], move[1]},  player)){
					return false;
				}
				
			}
		}
		if (move[1]-1>=0){  // check top
			if (board[move[0]][move[1]-1].isOccupied()&&board[move[0]][move[1]-1].player.equals(player.name)){
				if (checkRaidHelper(board, new int[] {move[0], move[1]},  player)){
					return false;
				}
				
			}
		}
		if (move[1]+1<=board.length-1){  // check bottom
			if (board[move[0]][move[1]+1].isOccupied()&&board[move[0]][move[1]+1].player.equals(player.name)){
				if(checkRaidHelper(board, new int[] {move[0], move[1]},  player)){
					return false;
				}
			}
		}
		return true;
	}*/
	private static boolean checkRaid(Node[][] board, int[] move, Player player){
		//check if there's own adjacent
		if (move[0]-1>=0){   // check left
			if (board[move[0]-1][move[1]].isOccupied()&&board[move[0]-1][move[1]].player.equals(player.name)){
				// should check adjacent 
				return checkRaidHelper(board, new int[] {move[0], move[1]},  player);
			}
		}
		if (move[0]+1<=board.length-1){  // check right
			if (board[move[0]+1][move[1]].isOccupied()&&board[move[0]+1][move[1]].player.equals(player.name)){
				return checkRaidHelper(board, new int[] {move[0], move[1]},  player);
				
			}
		}
		if (move[1]-1>=0){  // check top
			if (board[move[0]][move[1]-1].isOccupied()&&board[move[0]][move[1]-1].player.equals(player.name)){
				return checkRaidHelper(board, new int[] {move[0], move[1]},  player);
				
			}
		}
		if (move[1]+1<=board.length-1){  // check bottom
			if (board[move[0]][move[1]+1].isOccupied()&&board[move[0]][move[1]+1].player.equals(player.name)){
				return checkRaidHelper(board, new int[] {move[0], move[1]},  player);
			}
		}
		return false;
	}
	private static boolean checkRaidHelper(Node[][] board, int[] move, Player player){
		if (move[0]-1>=0){   // check left
			if (board[move[0]-1][move[1]].isOccupied()&&!board[move[0]-1][move[1]].player.equals(player.name)){
				return true;
			}
		}
		if (move[0]+1<=board.length-1){  // check right
			if (board[move[0]+1][move[1]].isOccupied()&&!board[move[0]+1][move[1]].player.equals(player.name)){
				return true;
			}
		}
		if (move[1]-1>=0){  // check top
			if (board[move[0]][move[1]-1].isOccupied()&&!board[move[0]][move[1]-1].player.equals(player.name)){
				return true;
			}
		}
		if (move[1]+1<=board.length-1){  // check bottom
			if (board[move[0]][move[1]+1].isOccupied()&&!board[move[0]][move[1]+1].player.equals(player.name)){
				return true;
			}
		}
		return false;
	}
	private static List<int[]> generateMoves(Node[][] board) {
		List<int[]> nextMoves = new ArrayList<int[]>(); // allocate List
		for (int row = 0; row < board.length; ++row) {
			for (int col = 0; col < board.length; ++col) {
				if (!board[row][col].isOccupied()) {
					nextMoves.add(new int[] {row, col});
				}
			}
		}
		return nextMoves;
	}
	private static int evaluate( Node[][] board, Player computer) {
	      int score = 0;
	      for (int i = 0;i<board.length;i++){
	    	  for (int j = 0; j<board.length;j++){
	    		  
	    		  if (board[i][j].isOccupied()&&board[i][j].player.equals(computer.name)){
	    			  score +=board[i][j].getValue();
	    		  }
	    		  if (board[i][j].isOccupied()&&!board[i][j].player.equals(computer.name)){
	    			  score -=board[i][j].getValue();
	    		  }
	    	  }
	      }
	      return score;
	   }
	
	
	private static void initializeBoardValue (int row, String[] value, Node[][] board, int size){
		for (int i = 0; i<size;i++){
			board[row][i] = new Node();
			board[row][i].setValue(Integer.valueOf(value[i]));
		}
	}
	private static void initializeBoardPlayer (int row, String[] value, Node[][] board, int size, Player ours, Player theirs){
		for (int i = 0; i<size;i++){
			if (value[i].equals("X")){
				board[row][i].initializePlayer((value[i]));
				if (ours.name.equals("X"))
					ours.addValue(board[row][i].getValue());
				else 
					theirs.addValue(board[row][i].getValue());
			}	
			if (value[i].equals("O")){
				board[row][i].initializePlayer(value[i]);
				if (ours.name.equals("O"))
					ours.addValue(board[row][i].getValue());
				else 
					theirs.addValue(board[row][i].getValue());
			}
		}
	}
	
	
	static class Node {
		int value;
		String player;
		boolean occupied;
		public Node (){
			value = 0;
			player = "";
			occupied = false;
		}
		void initializePlayer(String p){
			player = p;
			occupied = true;
		}
		void setPlayer(String p){
			player = p;
			occupied = true;
		}
		void setValue(int v){
			value = v;
		}
		boolean isOccupied(){
			return occupied;
		}
		boolean isMine(String p){
			return player.equals(p);
		}
		int getValue(){
			return value;
		}
		void setUnOccupied(){
			player = "";
			occupied = false;
		}
	}
	
	static class Player{
		String name;
		int value;
		public Player(String player){
			name = player;
			value = 0;
		}
		int getValue(){
			return value;
		}
		void addValue(int v){
			value +=v;
		}
	}
}

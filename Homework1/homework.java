import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class homework {
	private static PrintWriter out;
	private static File createFile;
	private static ArrayList<Node> result;
	private static ArrayList<NodeList> resultList;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "";
		try {
				readInputFile("input.txt");
		  	}
		  	catch (FileNotFoundException exc) {
		  		System.out.println("File not found: " + fileName);
		  	}
		  	catch (IOException exc) {
		  		exc.printStackTrace();
		  	}
	}
	private static void readInputFile(String fileName) throws IOException {
		HashMap<String, ArrayList<edgesObject>> outerMap = new HashMap<String, ArrayList<edgesObject>>();
		HashMap<String, Node> nodeSource = new HashMap<String, Node>();
		int count = 0;
		int distance = 0;
		String algo = "";
		int numLines = 0;
		int numTrafficLines = 0;
		String start = "";
		String goal = "";
		FileReader file = new FileReader(fileName);
		BufferedReader br = new BufferedReader(file);
		String line = null;
		// read first line 
		if ((line = br.readLine()) !=null){
		    String [] algorithm = line.split(" ");
		    algo = algorithm[0].trim().replace(" ", "");      //by reading first line, give the maze the row
		    //System.out.println(algo);
		}
		// read start node
		if ((line = br.readLine()) !=null){
			String [] token = line.split(" ");
			start = token[0].trim().replace(" ", "");
			Node node = new Node(start);
			node.setStart();
			nodeSource.put(node.getName(), node);
		}
		// read goal node
		if ((line = br.readLine()) !=null){
			String [] token = line.split(" ");
			goal = token[0].trim().replace(" ", "");
			Node node = new Node(goal);
			node.setGoal();
			if (nodeSource.containsKey(goal)){
				nodeSource.get(goal).setGoal();
			}
			else 
				nodeSource.put(node.getName(), node);
		}
		//read all edges that mentioned
		if ((line = br.readLine()) !=null){
			String [] numOfedges = line.split(" ");
			numLines = Integer.valueOf(numOfedges[0].trim().replace(" ", ""));
		    for (int i =0;i<numLines;i++){
		    	// to store edges 
		    	if ((line = br.readLine()) !=null){
		    		String [] edges = line.split(" ");
			    	String edgeStart = edges[0].trim().replace(" ", "");
			    	if (!nodeSource.containsKey(edgeStart)){
			    		nodeSource.put(edgeStart, new Node(edgeStart));
			    	}
			    	String edgeEnd = edges[1].trim().replace(" ", "");
			    	if (!nodeSource.containsKey(edgeEnd)){
			    		nodeSource.put(edgeEnd, new Node(edgeEnd));
			    	}
			    	String tmp = edges[2].trim();
			    	int time_edge = Integer.valueOf(tmp.replace(" ", ""));
			    	edgesObject edgeObj = new edgesObject(edgeEnd, time_edge);
			    	
			    	if (outerMap.containsKey(edgeStart))
			    		outerMap.get(edgeStart).add(edgeObj);
			    	else {
			    		outerMap.put(edgeStart, new ArrayList<edgesObject>());
			    		outerMap.get(edgeStart).add(edgeObj);
			    	}
		    	}
		    }	
		}
		// read all nodes with estimated time 
		if ((line = br.readLine()) !=null){
			String [] numOfnodes = line.split(" ");
			numTrafficLines = Integer.valueOf(numOfnodes[0].trim().replace(" ", ""));
		    for (int i =0;i<numTrafficLines;i++){
		    	if ((line = br.readLine()) !=null){
		    		String [] nodesName = line.split(" ");
		    		String NodeKey = nodesName[0].trim().replace(" ", "");
		    		if (!nodeSource.containsKey(NodeKey)){
		    			Node node = new Node(NodeKey);
		    			String tmp = nodesName[1].trim();
		    			int tmp_cost = Integer.valueOf(tmp.replace(" ", ""));
		    			node.setTrafficCost(tmp_cost);
		    			//nodeArr.add(node);
		    			nodeSource.put(node.getName(), node);
		    		}
		    		else {
		    			String tmp = nodesName[1].trim();
		    			int tmp_cost = Integer.valueOf(tmp.replace(" ", ""));
		    			nodeSource.get(NodeKey).setTrafficCost(tmp_cost);
		    		}
		    	}
		    }
	    }
		br.close();	
		// choose algorithms 
		try {
			createFile = new File ("output.txt");
			out = new PrintWriter(createFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switch (algo){
		
			case "BFS":
				result = bfs(outerMap, nodeSource, start);
				count = 0;
				for (int i = 0;i<result.size();i++){
					out.println(result.get(i).getName() + " " + count++);
				}
				out.close();
				break;
			case "DFS": 
				result = dfs(outerMap, nodeSource, start);
				count = 0;
				for (int i = 0;i<result.size();i++){
					out.println(result.get(i).getName() + " " + count++);
				}
				out.close();
				break;
			case "UCS": 
				resultList = ucs(outerMap, nodeSource, start);
				distance = 0;
				for (int i = 0; i<resultList.get(0).size()-1;i++){
					out.println(resultList.get(0).get(i).getName() + " " + distance);
					for (int j = 0; j < outerMap.get(resultList.get(0).get(i).getName()).size(); j++){
						if (outerMap.get(resultList.get(0).get(i).getName()).get(j).destination.equals(resultList.get(0).get(i+1).getName())){	
							distance += outerMap.get(resultList.get(0).get(i).getName()).get(j).distance;
						}
					}
				}
				out.println(resultList.get(0).get(resultList.get(0).size()-1).getName() + " " + distance);
				out.close();
				break;
			case "A*": 
				resultList = a_star(outerMap, nodeSource, start);
				
				for (int i = 0; i<resultList.get(0).size()-1;i++){
					out.println(resultList.get(0).get(i).getName() + " " + distance);
					for (int j = 0; j < outerMap.get(resultList.get(0).get(i).getName()).size(); j++){
						if (outerMap.get(resultList.get(0).get(i).getName()).get(j).destination.equals(resultList.get(0).get(i+1).getName())){	
							distance += outerMap.get(resultList.get(0).get(i).getName()).get(j).distance;
						}
					}
				}
				out.println(resultList.get(0).get(resultList.get(0).size()-1).getName() + " " + distance);
				out.close();
				break;
		}
	}
	private static ArrayList<Node> bfs(HashMap<String, ArrayList<edgesObject>> outerMap, HashMap<String, Node> nodeSource, String start){
		Queue<Node> queue = new LinkedList<Node>();
		queue.add(nodeSource.get(start));   // put start node into queue first
		while (!queue.peek().isEnd()){
			queue.peek().setVisited(true);
			Node currentNode = queue.poll();
			if (outerMap.containsKey(currentNode.getName())){
				for (int i = 0; i<outerMap.get(currentNode.getName()).size();i++){
					if (nodeSource.containsKey(outerMap.get(currentNode.getName()).get(i).destination)){
						if (!nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).isVisited()){
							nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).setParent(currentNode);
							nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).setVisited(true); // to prevent from second encounter 
							queue.add(nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination));
						}
					}
				}
			}
		}
		Node currentNode = queue.poll();
		ArrayList<Node> result = new ArrayList<Node>();
		Node tempNode = currentNode;
		result.add(tempNode);
		while (!tempNode.isStart()){
			 tempNode = tempNode.getParent();
			 result.add(tempNode);
		}
		Collections.reverse(result);
		return result;
	}
	private static ArrayList<Node> dfs(HashMap<String, ArrayList<edgesObject>> outerMap, HashMap<String, Node> nodeSource, String start){
		Stack<Node> stack = new Stack<>();
		stack.push(nodeSource.get(start));   // put start node into queue first
		while (!stack.peek().isEnd()){
			stack.peek().setVisited(true);
			Node currentNode = stack.pop();
			if (outerMap.containsKey(currentNode.getName())){   // if has child nodes 
				for (int i = outerMap.get(currentNode.getName()).size()-1; i>=0; i--){
					if (nodeSource.containsKey(outerMap.get(currentNode.getName()).get(i).destination)){
						if (!nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).isVisited()){
							nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).setVisited(true);
							nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).setParent(currentNode);
							//nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).setStep(nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).getParent().getStep()+1);
							//System.out.println("push " + nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination).getName());
							stack.push(nodeSource.get(outerMap.get(currentNode.getName()).get(i).destination));
						}
					}
				}
			}
		}
		Node currentNode = stack.pop();
		ArrayList<Node> result = new ArrayList<Node>();
		Node tempNode = currentNode;
		result.add(tempNode);
		while (!tempNode.isStart()){
			 tempNode = tempNode.getParent();
			 result.add(tempNode);
		}
		Collections.reverse(result);
		return result;
	}
	
	private static ArrayList<NodeList> a_star(HashMap<String, ArrayList<edgesObject>> outerMap, HashMap<String, Node> nodeSource, String start){
		ArrayList<NodeList> result = new ArrayList<NodeList>();
		PriorityQueue<NodeList> priorityQ = new PriorityQueue<NodeList>(nodeSource.size(), new Comparator<NodeList>(){
			@Override
			public int compare(NodeList list1, NodeList list2) {
				// TODO Auto-generated method stub
				if(list1.getCost() == list2.getCost()){
					return (list1.getOrdered() - list2.getOrdered());
				}else{
					return (list1.getCost() - list2.getCost());					
				}
			}			
		});
		NodeList nodeList = new NodeList();  // find the start state
		nodeList.add(nodeSource.get(start));
		nodeList.addCost(nodeSource.get(start).getTrafficCost());
		priorityQ.add(nodeList); 
		int count = 0;
		while(!priorityQ.peek().isGoal()){
			NodeList currentNodeList = priorityQ.poll();
			if (outerMap.containsKey(currentNodeList.getLastNode().getName())){
				for (int i = 0; i<outerMap.get(currentNodeList.getLastNode().getName()).size();i++){ // to get child for currentNode
					if (nodeSource.containsKey(outerMap.get(currentNodeList.getLastNode().getName()).get(i).destination)){    //to run a loop to count how many child they have
						
						if (!currentNodeList.isVisitedinList(nodeSource.get(outerMap.get(currentNodeList.getLastNode().getName()).get(i).destination))){   // more than one child meaning there should be another nodeList storing into PriorityQ
							NodeList newNodeList = new NodeList();
							newNodeList.addAll(currentNodeList);
							newNodeList.addCost(currentNodeList.getCost());
							newNodeList.addCost(outerMap.get(currentNodeList.getLastNode().getName()).get(i).distance 
									+ nodeSource.get(outerMap.get(currentNodeList.getLastNode().getName()).get(i).destination).getTrafficCost()
									- currentNodeList.getLastNode().getTrafficCost()); // add new h(n) but minus h(n-1) 
							
							newNodeList.add(nodeSource.get(outerMap.get(currentNodeList.getLastNode().getName()).get(i).destination));
							newNodeList.setOrdered(count);
							priorityQ.add(newNodeList);
							count++;
						}
					}
				}
			}
		}
		NodeList currentNodeList = priorityQ.poll();
		result.add(currentNodeList);
		//System.out.println(result.size());
		/*Collections.sort(result, new Comparator<NodeList>(){
			@Override
	        public int compare(NodeList list1, NodeList list2)
	        {
				if (list1.getCost() == list2.getCost()){
					return list1.getOrdered() - list2.getOrdered();
				}
				else 
					return  list1.getCost() - list2.getCost();
	        }
		});*/
		
		return result;
	}
	private static ArrayList<NodeList> ucs(HashMap<String, ArrayList<edgesObject>> outerMap, HashMap<String, Node> nodeSource, String start){
		ArrayList<NodeList> result = new ArrayList<NodeList>();
		
		PriorityQueue<NodeList> priorityQ = new PriorityQueue<NodeList>(nodeSource.size(), new Comparator<NodeList>(){
			@Override
			public int compare(NodeList list1, NodeList list2) {
				// TODO Auto-generated method stub
				if(list1.getCost() == list2.getCost()){
					return list1.getOrdered() - (list2.getOrdered());
				}else{
					return (list1.getCost() - list2.getCost());					
				}
			}			
		});
		// find the start state
		NodeList nodeList = new NodeList();  // find the start state
		nodeList.add(nodeSource.get(start));
		priorityQ.add(nodeList);
		int count = 0;    // to count order 
		while(!priorityQ.peek().isGoal()){
			NodeList currentNodeList = priorityQ.poll();
			if (outerMap.containsKey(currentNodeList.getLastNode().getName())){
				for (int i = 0; i<outerMap.get(currentNodeList.getLastNode().getName()).size();i++){ // to get child for currentNode
					if (nodeSource.containsKey(outerMap.get(currentNodeList.getLastNode().getName()).get(i).destination)){    //to run a loop to count how many child they have
						if (!currentNodeList.isVisitedinList(nodeSource.get(outerMap.get(currentNodeList.getLastNode().getName()).get(i).destination))){   // more than one child meaning there should be another nodeList storing into PriorityQ
							NodeList newNodeList = new NodeList();
							newNodeList.addAll(currentNodeList);
							newNodeList.addCost(currentNodeList.getCost());
							newNodeList.addCost(outerMap.get(currentNodeList.getLastNode().getName()).get(i).distance); // add new h(n) but minus h(n-1) 
							newNodeList.add(nodeSource.get(outerMap.get(currentNodeList.getLastNode().getName()).get(i).destination));
							newNodeList.setOrdered(count);
							priorityQ.add(newNodeList);
							count++;
						}
					}
				}
			}
		}
		NodeList currentNodeList = priorityQ.poll();
		result.add(currentNodeList);
		/*Collections.sort(result, new Comparator<NodeList>(){
			@Override
	        public int compare(NodeList list1, NodeList list2)
	        {
				if (list1.getCost() == list2.getCost()){
					return list1.getOrdered() - list2.getOrdered();
				}
				else 
					return  list1.getCost() - list2.getCost();
	        }
		});*/
		return result;
	}
	static class edgesObject {
		String destination;
		int distance;
		public edgesObject (String Dest, int Dis){
			destination = Dest;
			distance = Dis;
		}
	}
	static class NodeList {
		ArrayList<Node> nodeListVisited;
		ArrayList<Node> nodeList;
		int totCost;
		int ordered;
		public NodeList(){
			nodeList = new ArrayList<Node>();
			nodeListVisited = new ArrayList<Node>();
			totCost = 0;
			ordered = 0;
		}
		/*public NodeList(NodeList list){
			nodeList = new ArrayList<Node>();
			nodeListVisited = new ArrayList<Node>();
			totCost = 0;
			ordered = 0;
			for (int i = 0; i<list.size();i++){
				nodeList.add(list.get(i));
			}
		}*/
		public void addAll(NodeList list){
			for (int i = 0; i<list.size();i++){
				nodeList.add(list.get(i));
				nodeListVisited.add(list.get(i));
			}
		}
		public void add(Node node){
			nodeList.add(node);
			nodeListVisited.add(node);
		}
		public boolean isGoal(){
			return nodeList.get((nodeList.size()-1)).isEnd();
		}
		public void addCost(int cost){
			totCost = totCost + cost;
		}
		public int getCost(){
			return totCost;
		}
		public int getOrdered(){
			return ordered;
		}
		public void setOrdered(int number){
			ordered = number;
		}
		public Node getLastNode(){
			return nodeList.get((nodeList.size()-1));
		}
		public int size(){
			return nodeList.size();
		}
		public Node get(int index){
			return nodeList.get(index);
		}
		public boolean isVisitedinList(Node node){
			return nodeListVisited.contains(node);
		}
	}
	static class Node {
		int unit_step;
		String name;
		Node parentNode;
		int cost_time;
		int traffic_cost_time;
		int a_star_tot_time;
		boolean start;
		boolean end;
		boolean visited;
		public Node (String Name){
			name = Name;
			unit_step = 0;
			cost_time = 0;
			traffic_cost_time = 0;
			a_star_tot_time = 0;
			start = false;
			end = false;
			visited = false;
		}
		public String getName(){
			return name;
		}
		public boolean setCost(int cost){
			if(cost_time < cost){
				cost_time = cost;
				return true;
			}else{
				return false;
			}
		}
		public int getCost(){
			return cost_time;
		}
		public void setTrafficCost(int cost){
			traffic_cost_time = cost;
		}
		public int getTrafficCost(){
			return traffic_cost_time;
		}
		public void setTotalCostAStar(int cost){
			a_star_tot_time = cost;
		}
		public int getTotalCostAStar(){
			return a_star_tot_time;
		}
		public void setStart(){
			start = true;
		}
		public void setGoal(){
			end = true;
		}
		public String toString(){
			return name + " " + cost_time;
		}
		public boolean isStart(){
			return start;
		}
		public boolean isEnd(){
			return end;
		}
		public boolean isVisited(){
			return visited;
		}
		public void setVisited(boolean visit){
			visited = visit;
		}
		public void setParent(Node parent){
			parentNode = parent;
		}
		public Node getParent(){
			return parentNode;
		}
	}
	
}
